// var RestServiceModule =  angular.module('RestServiceModule',['restangular']);
var base_url = 'http://localhost:8080/api';
myapp.service('RestMethodsService', function(Restangular) {
    Restangular.setBaseUrl(base_url);

    this.getUsers = function() {
        var students = Restangular.all('all-students');
        var data = students.getList();
        return data;
    }
    this.findOneStudent = function(studentId) {
        return Restangular.one('student', studentId).get();
    }
    this.createStudent = function(data) {
        var students = Restangular.all('add');
        var postInfo = students.post(data);
        return postInfo;
    }
    this.updateStudent = function(studentId, data) {
        var putStudent = Restangular.one('update', studentId);
        return putStudent.customPUT(data);
    }
    this.terminateStudent = function(studentId) {
        return Restangular.one('delete', studentId).put();
    }
});