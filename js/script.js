var myapp = angular.module('appModule', ['restangular', 'ngRoute'])
    .config(function($routeProvider) {
        $routeProvider
            .when("/home", {
                templateUrl: "Templates/home.html",
                controller: "displayController"
            })
            .when("/add", {
                templateUrl: "Templates/add.html",
                controller: "addController"
            })
            .when("/update/:studentId", {
                templateUrl: "Templates/update.html",
                controller: "updateController"
            })
    })
    .controller('displayController', ['RestMethodsService', '$scope', '$location', function(RestMethodsService, $scope, $location) {
        RestMethodsService.getUsers().then(function(data) {
            $scope.dataset = data;
            console.log(data);
        }, function() {
            console.log("error");
        });
        $scope.deleteRecord = function(student) {
            RestMethodsService.terminateStudent(student.studentId).then(function() {
                console.log("Terminated Successfully!")
                $location.url('#!home');
            }, function() {
                console.log("error");
            })
        }
    }])
    .controller('addController', ['RestMethodsService', '$scope', function(RestMethodsService, $scope) {
        $scope.add = function(student) {
            RestMethodsService.createStudent(student).then(function(data) {
                console.log("form Submitted Successfully" + data);
            }, function() {
                console.log("error in form submission");
            });
        }
    }])
    .controller('updateController', ['RestMethodsService', '$scope', '$routeParams','$location', function(RestMethodsService, $scope, $routeParams, $location) {
        var studentId = $routeParams.studentId;
        $scope.oldStudent = RestMethodsService.findOneStudent(studentId).then(function(response){
            $scope.student = {};
            $scope.student.fullName =  response.fullName;
            $scope.student.level =  response.level;
            $scope.student.phoneNumber =  response.phoneNumber;
        });

        $scope.update = function(student) {
            RestMethodsService.updateStudent(studentId, student).then(function() {
                console.log("data updated successfully!!")
                $location.url('#!home');
            }, function() {
                console.log("error in data update")
            });
        }
    }])